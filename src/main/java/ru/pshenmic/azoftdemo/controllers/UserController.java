package ru.pshenmic.azoftdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.pshenmic.azoftdemo.api.UserApi;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;
import ru.pshenmic.azoftdemo.service.MappingService;
import ru.pshenmic.azoftdemo.service.UserService;
import ru.pshenmic.azoftdemo.service.ValidationService;

import java.util.List;

@Controller
public class UserController implements UserApi {


    private final UserService userService;

    private final ValidationService validationService;

    private final MappingService mappingService;

    @Autowired
    public UserController(UserService userService, ValidationService validationService, MappingService mappingService) {
        this.userService = userService;
        this.validationService = validationService;
        this.mappingService = mappingService;
    }


    @Override
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {

        if (!validationService.validateUserDTOForCreate(userDTO)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User user = userService.getUserByLogin(userDTO.getLogin());

        if(user != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        user = userService.createUser(userDTO);

        return new ResponseEntity<>(mappingService.userToUserDTO(user), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<User> users = userService.getAllUsers();

        return new ResponseEntity<>(mappingService.userListToDTOList(users), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        User user = userService.getUserById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mappingService.userToUserDTO(user), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> modifyUserById(@PathVariable Long id, @RequestBody UserDTO userDTO) {
        User user = userService.getUserById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!validationService.validateUserDTO(userDTO)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        user = userService.updateUser(user, userDTO);

        return new ResponseEntity<>(mappingService.userToUserDTO(user), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteUserById(@PathVariable Long id) {
        User user = userService.getUserById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userService.deleteById(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDTO> getAuthenticatedUser() {
        if (!isAnonymous()) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return new ResponseEntity<>(mappingService.userToUserDTO(user), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<UserDTO> modifyAuthenticatedUser(@RequestBody UserDTO userDTO) {
        if (!isAnonymous()) {

            if (!validationService.validateUserDTO(userDTO)) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            user = userService.updateUser(user, userDTO);
            return new ResponseEntity<>(mappingService.userToUserDTO(user), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }


    private boolean isAnonymous() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream()
                .anyMatch(role ->
                        role.getAuthority().equals("ROLE_ANONYMOUS"));
    }

}
