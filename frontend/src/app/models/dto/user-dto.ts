export interface UserDto {
  id: number;
  firstName: string;
  lastName: string;
  dateOfBirth: Date
  login: string
  about: string;
  address: string;
  role: string;
  password: string;
}
