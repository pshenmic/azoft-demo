## AzoftDemo 1.0-SNAPSHOT

Time spent: ~25-35 hours

Functionality not included in this release:
1) Pagination
2) Service layer tests
3) Fields with error highlighting
4) Swagger-UI for RestAPI documentation
5) Frontend tests
6) Logging to file + rotating
7) WAR package (for Tomcat servers). Docker-compose file instead.

### Pre-run

Before running the application you have to specify your own database configuration. Either modify application-{profile}.properties manually or pass overriding keys through -D in maven.


###Dev mode

1) ```cd frontend && npm install && npm run watch```
2) ```cd ..```
3) ```mvn clean install -Pdev```
4) ```mvn spring-boot:run -Dspring.profiles.active=dev``` Or just import project into your favorite IDE.


###Prod mode

1) ```mvn clean install -Pprod```
2) ```java -jar target/azoft-demo-1.0-SNAPSHOT.jar --spring.profiles.active=prod```


### Run in docker containers

Very good behaviour for production systems. Everything is inside containers and the data persisted to the data directory.

1) ```cd docker/compose```
2) ```docker-compose up```