package ru.pshenmic.azoftdemo.mapstruct;


import org.mapstruct.Mapper;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserListMapper {

    List<UserDTO> userListToDTOList(List<User> user);

}
