package ru.pshenmic.azoftdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pshenmic.azoftdemo.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);

}
