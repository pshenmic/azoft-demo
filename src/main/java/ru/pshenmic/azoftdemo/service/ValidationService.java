package ru.pshenmic.azoftdemo.service;


import org.springframework.stereotype.Service;
import ru.pshenmic.azoftdemo.dto.UserDTO;

@Service
public class ValidationService {


    public boolean validateUserDTO(UserDTO userDTO) {
        //Add your validation rules here
        return true;
    }

    public boolean validateUserDTOForCreate(UserDTO userDTO) {
        //Add your validation rules here
        return userDTO.getLogin()!=null && userDTO.getPassword() != null && userDTO.getRole() != null;
    }

}
