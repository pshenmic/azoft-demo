export interface AuthCheckDto {
  authenticated: boolean;
  role: string;
}
