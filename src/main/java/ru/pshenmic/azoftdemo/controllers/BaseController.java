package ru.pshenmic.azoftdemo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class BaseController {

    //Let Angular handle the routing
    //You must return /index.html on angular routes
    //Another, even better solution is to make handler for any 404
    //And then return index.html if path DOES NOT matches /api.
    //404 instead
    @RequestMapping(value = {"login", "admin", "admin/dashboard", "home", "admin/user/edit/{id}", "admin/user/create"})
    public String forwardToIndex() {
        return "forward:/index.html";
    }

}
