package controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.pshenmic.azoftdemo.AzoftDemo;
import ru.pshenmic.azoftdemo.configuration.WebSecurityConfiguration;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;
import ru.pshenmic.azoftdemo.repository.UserRepository;
import ru.pshenmic.azoftdemo.service.MappingService;
import ru.pshenmic.azoftdemo.service.UserService;
import ru.pshenmic.azoftdemo.service.ValidationService;

import java.time.LocalDate;
import java.util.Base64;

import static junit.framework.TestCase.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes =
                {AzoftDemo.class, WebSecurityConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private MappingService mappingService;

    private MockMvc restMockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private final static String admin_firstName = "admin_first_name";
    private final static String admin_lastName = "admin_last_name";
    private final static String admin_login = "admin";
    private final static String admin_password = new BCryptPasswordEncoder().encode("admin");
    private final static LocalDate admin_dateOfBirth = LocalDate.now();
    private final static String admin_about = "admin";
    private final static String admin_address = "admin";
    private final static String admin_role = "ROLE_ADMIN";

    private final static String user_firstName = "user_first_name";
    private final static String user_lastName = "user_last_name";
    private final static String user_login = "user";
    private final static String user_password = new BCryptPasswordEncoder().encode("user");
    private final static LocalDate user_dateOfBirth = LocalDate.now();
    private final static String user_about = "user";
    private final static String user_address = "user";
    private final static String user_role = "ROLE_USER";

    @Before
    public void setup() {
        this.restMockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();

        prepareTestSuit();
    }

    private void prepareTestSuit() {
        userRepository.deleteAll();

        User admin = new User();
        User user = new User();

        admin.setFirstName(admin_firstName);
        admin.setLastName(admin_lastName);
        admin.setLogin(admin_login);
        admin.setDateOfBirth(admin_dateOfBirth);
        admin.setAddress(admin_address);
        admin.setAbout(admin_about);
        admin.setRole("ROLE_ADMIN");
        admin.setPassword(admin_password);


        user.setFirstName(user_firstName);
        user.setLastName(user_lastName);
        user.setLogin(user_login);
        user.setDateOfBirth(user_dateOfBirth);
        user.setAddress(user_address);
        user.setAbout(user_about);
        user.setRole(user_role);
        user.setPassword(user_password);

        userRepository.save(admin);
        userRepository.save(user);
    }


    @Test
    public void testCreateUserByAdmin() throws Exception {
        assertTrue(userRepository.count() == 2);

        String firstName = "first_name";
        String lastName = "last_name";
        String login = "login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "address";
        String about = "About";
        String password = "user";
        String role = "ROLE_USER";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(login);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole(role);
        userDTO.setPassword(password);
        userDTO.setAbout(about);
        userDTO.setAddress(address);

        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.login").value(login))
                .andExpect(jsonPath("$.address").value(address))
                .andExpect(jsonPath("$.about").value(about))
                .andExpect(jsonPath("$.role").value(role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        userDTO = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), UserDTO.class);
        assertTrue(userDTO.getDateOfBirth().compareTo(dateOfBirth) == 0);
        assertTrue(userRepository.count() == 3);
    }

    @Test
    public void testCreateInvalidUserByAdmin() throws Exception {
        assertTrue(userRepository.count() == 2);

        String firstName = "first_name";
        String lastName = "last_name";
        String login = "login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "address";
        String about = "About";
        String password = "user";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);

        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }


    @Test
    public void testGetClientById() throws Exception {
        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("admin"))
                .findFirst()
                .orElseThrow(Exception::new);

            MvcResult mvcResult = restMockMvc
                    .perform(MockMvcRequestBuilders.get("/api/user/" + user.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes()))))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").isNumber())
                    .andExpect(jsonPath("$.firstName").value(admin_firstName))
                    .andExpect(jsonPath("$.lastName").value(admin_lastName))
                    .andExpect(jsonPath("$.login").value(admin_login))
                    .andExpect(jsonPath("$.address").value(admin_address))
                    .andExpect(jsonPath("$.about").value(admin_about))
                    .andExpect(jsonPath("$.role").value(admin_role))
                    .andExpect(jsonPath("$.password").isEmpty())
                    .andReturn();


        UserDTO userDTO = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), UserDTO.class);
        assertTrue(userDTO.getDateOfBirth().compareTo(admin_dateOfBirth) == 0);
    }

    @Test
    public void testGetClientByUnknownId() throws Exception {
        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/user/1337")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes()))))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }



    @Test
    public void testModifyUserByIdAllFields() throws Exception {
        assertTrue(userRepository.count() == 2);

        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);

        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String login = "new_login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "new_address";
        String about = "new_about";
        String password = "new_user";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);

        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.login").value(user_login))
                .andExpect(jsonPath("$.about").value(about))
                .andExpect(jsonPath("$.address").value(address))
                .andExpect(jsonPath("$.role").value(user_role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        assertTrue(userDTO.getDateOfBirth().compareTo(dateOfBirth) == 0);

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testModifyUserByIdSomeFields() throws Exception {
        assertTrue(userRepository.count() == 2);

        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);

        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String about = "new_about";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(admin_dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(null);

        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.login").value(user_login))
                .andExpect(jsonPath("$.about").value(about))
                .andExpect(jsonPath("$.address").value(user_address))
                .andExpect(jsonPath("$.role").value(user_role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        assertTrue(userDTO.getDateOfBirth().compareTo(admin_dateOfBirth) == 0);

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testModifyUserByUnknownId() throws Exception {
        assertTrue(userRepository.count() == 2);

        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String about = "new_about";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(admin_dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(null);

        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user/1337")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }



    @Test
    public void testDeleteById() throws Exception {
        assertTrue(userRepository.count() == 2);

        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.delete("/api/user/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes()))))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(userRepository.count() == 1);
    }

    @Test
    public void testDeleteByUnknown() throws Exception {
        assertTrue(userRepository.count() == 2);

        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.delete("/api/user/1337")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes()))))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testGetCurrentUser() throws Exception {
        assertTrue(userRepository.count() == 2);


        //login as user

        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("user:user".getBytes()))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(user_firstName))
                .andExpect(jsonPath("$.lastName").value(user_lastName))
                .andExpect(jsonPath("$.login").value(user_login))
                .andExpect(jsonPath("$.about").value(user_about))
                .andExpect(jsonPath("$.address").value(user_address))
                .andExpect(jsonPath("$.role").value(user_role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testGetCurrentUserNoAuth() throws Exception {
        assertTrue(userRepository.count() == 2);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }
    @Test
    public void testGetCurrentUserBadCredentials() throws Exception {
        assertTrue(userRepository.count() == 2);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("random:string".getBytes()))))
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testModifyCurrentUserAllFieldsNoAuth() throws Exception {
        assertTrue(userRepository.count() == 2);


        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String login = "new_login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "new_address";
        String about = "new_about";
        String password = "new_user";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);



        //login as user
        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testModifyCurrentUserAllFieldsBadCredentials() throws Exception {
        assertTrue(userRepository.count() == 2);


        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String login = "new_login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "new_address";
        String about = "new_about";
        String password = "new_user";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);



        //login as user
        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userDTO))
                .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("random:string".getBytes()))))
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }
    @Test
    public void testModifyCurrentUserAllFields() throws Exception {
        assertTrue(userRepository.count() == 2);


        String firstName = "new_first_name";
        String lastName = "new_last_name";
        String login = "new_login";
        LocalDate dateOfBirth = LocalDate.now();
        String address = "new_address";
        String about = "new_about";
        String password = "new_user";

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin(null);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);



        //login as user
        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("user:user".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.login").value(user_login))
                .andExpect(jsonPath("$.about").value(about))
                .andExpect(jsonPath("$.address").value(address))
                .andExpect(jsonPath("$.role").value(user_role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testModifyCurrentUserSomeFields() throws Exception {
        assertTrue(userRepository.count() == 2);

        String firstName = "first_name";
        String lastName = "second_name";
        String login = null;
        LocalDate dateOfBirth = null;
        String address = null;
        String about = user_about;
        String password = null;

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setLogin("asdasdasd");
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setRole("ROLE_USER");
        userDTO.setPassword(null);
        userDTO.setAbout(about);
        userDTO.setAddress(address);



        //login as user
        User user = userRepository.findAll()
                .stream()
                .filter(user1 -> user1.getLogin().equals("user"))
                .findFirst()
                .orElseThrow(Exception::new);


        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("user:user".getBytes())))
                        .content(objectMapper.writeValueAsString(userDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.login").value(user_login))
                .andExpect(jsonPath("$.about").value(user_about))
                .andExpect(jsonPath("$.address").value(user_address))
                .andExpect(jsonPath("$.role").value(user_role))
                .andExpect(jsonPath("$.password").isEmpty())
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

}
