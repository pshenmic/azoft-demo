package ru.pshenmic.azoftdemo.api;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pshenmic.azoftdemo.dto.AuthCheckDTO;
import ru.pshenmic.azoftdemo.dto.UserDTO;

import java.util.List;

@RequestMapping(value = "/api")
public interface AuthApi {


    /**
     * Allowed roles : [ROLE_ANONYMOUS, ROLE_USER, ROLE_ADMIN]
     *
     * Returns {AuthCheckDTO} of the authenticated\non-authenticated user
     *
     * 200 : {AuthCheckDTO} DTO with authenticated flag and role
     * 401 : {null} Returns on BadCredentials
     *
     * @return {AuthCheckDTO}
     */
    @RequestMapping(method = RequestMethod.GET, value = "/check")
    ResponseEntity<AuthCheckDTO> getAuth();


}
