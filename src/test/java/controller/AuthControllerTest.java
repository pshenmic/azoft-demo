package controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.pshenmic.azoftdemo.AzoftDemo;
import ru.pshenmic.azoftdemo.configuration.WebSecurityConfiguration;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;
import ru.pshenmic.azoftdemo.repository.UserRepository;
import ru.pshenmic.azoftdemo.service.MappingService;
import ru.pshenmic.azoftdemo.service.UserService;
import ru.pshenmic.azoftdemo.service.ValidationService;

import java.time.LocalDate;
import java.util.Base64;

import static junit.framework.TestCase.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes =
                {AzoftDemo.class, WebSecurityConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AuthControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc restMockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private final static String admin_firstName = "admin_first_name";
    private final static String admin_lastName = "admin_last_name";
    private final static String admin_login = "admin";
    private final static String admin_password = new BCryptPasswordEncoder().encode("admin");
    private final static LocalDate admin_dateOfBirth = LocalDate.now();
    private final static String admin_about = "admin";
    private final static String admin_address = "admin";
    private final static String admin_role = "ROLE_ADMIN";

    private final static String user_firstName = "user_first_name";
    private final static String user_lastName = "user_last_name";
    private final static String user_login = "user";
    private final static String user_password = new BCryptPasswordEncoder().encode("user");
    private final static LocalDate user_dateOfBirth = LocalDate.now();
    private final static String user_about = "user";
    private final static String user_address = "user";
    private final static String user_role = "ROLE_USER";

    @Before
    public void setup() {
        this.restMockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
                .build();

        prepareTestSuit();
    }

    private void prepareTestSuit() {
        userRepository.deleteAll();

        User admin = new User();
        User user = new User();

        admin.setFirstName(admin_firstName);
        admin.setLastName(admin_lastName);
        admin.setLogin(admin_login);
        admin.setDateOfBirth(admin_dateOfBirth);
        admin.setAddress(admin_address);
        admin.setAbout(admin_about);
        admin.setRole("ROLE_ADMIN");
        admin.setPassword(admin_password);


        user.setFirstName(user_firstName);
        user.setLastName(user_lastName);
        user.setLogin(user_login);
        user.setDateOfBirth(user_dateOfBirth);
        user.setAddress(user_address);
        user.setAbout(user_about);
        user.setRole(user_role);
        user.setPassword(user_password);

        userRepository.save(admin);
        userRepository.save(user);
    }


    @Test
    public void testAuthCheckAdmin() throws Exception {
        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/auth/check")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("admin:admin".getBytes()))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.authenticated").value(true))
                .andExpect(jsonPath("$.role").value("ROLE_ADMIN"))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testAuthCheckUser() throws Exception {
        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/auth/check")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .header("Authorization", "Basic " + new String(Base64.getEncoder().encode("user:user".getBytes()))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.authenticated").value(true))
                .andExpect(jsonPath("$.role").value("ROLE_USER"))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testAuthCheckNoAuth() throws Exception {
        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/auth/check")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.authenticated").value(false))
                .andExpect(jsonPath("$.role").value("ROLE_ANONYMOUS"))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }

    @Test
    public void testAuthCheckBadCredentials() throws Exception {
        MvcResult mvcResult = restMockMvc
                .perform(MockMvcRequestBuilders.get("/api/auth/check")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.authenticated").value(false))
                .andExpect(jsonPath("$.role").value("ROLE_ANONYMOUS"))
                .andReturn();

        assertTrue(userRepository.count() == 2);
    }


}
