package ru.pshenmic.azoftdemo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pshenmic.azoftdemo.api.AuthApi;
import ru.pshenmic.azoftdemo.dto.AuthCheckDTO;
import ru.pshenmic.azoftdemo.dto.UserDTO;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/auth")
public class AuthController implements AuthApi{

    private final static Logger log = LoggerFactory.getLogger(AuthController.class);

    @Override
    public ResponseEntity<AuthCheckDTO> getAuth() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorityList = authentication.getAuthorities();

        Optional<AuthCheckDTO> authCheckDTOOptional = authorityList.stream()
                .findFirst()
                .map(role -> mapToAuthCheckDTO(!role.getAuthority().equals("ROLE_ANONYMOUS"), role.getAuthority()));

        if (authCheckDTOOptional.isPresent()) {
            return new ResponseEntity<>(authCheckDTOOptional.get(), HttpStatus.OK);
        } else {
            log.error("Unexpected number of roles for user {}", authentication.getPrincipal());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private AuthCheckDTO mapToAuthCheckDTO(boolean authenticated, String role) {
        AuthCheckDTO authCheckDTO = new AuthCheckDTO();
        authCheckDTO.setAuthenticated(authenticated);
        authCheckDTO.setRole(role);
        return authCheckDTO;
    }

}
