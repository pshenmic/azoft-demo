package ru.pshenmic.azoftdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;
import ru.pshenmic.azoftdemo.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public User createUser(UserDTO userDTO) {
        User user = new User();
        user = writeAllFields(user, userDTO);
        return userRepository.save(user);
    }

    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    @Transactional
    public void deleteById(User user) {
        userRepository.delete(user);
    }

    @Transactional
    public User updateUser(User user, UserDTO userDTO) {
        user = writeModifiedFieldsForUser(user, userDTO);
        return userRepository.save(user);
    }

    private User writeModifiedFieldsForUser(User user, UserDTO userDTO) {
        if (userDTO.getFirstName() != null && (user.getFirstName() == null || !user.getFirstName().equals(userDTO.getFirstName()))) {
            user.setFirstName(userDTO.getFirstName());
        }
        if (userDTO.getLastName() != null && (user.getLastName() == null || !user.getLastName().equals(userDTO.getLastName()))) {
            user.setLastName(userDTO.getLastName());
        }
        if (userDTO.getDateOfBirth() != null && (user.getDateOfBirth() == null || user.getDateOfBirth().compareTo(userDTO.getDateOfBirth()) != 0)) {
            user.setDateOfBirth(userDTO.getDateOfBirth());
        }
        if (userDTO.getAbout() != null && (user.getAbout() == null || !user.getAbout().equals(userDTO.getAbout()))) {
            user.setAbout(userDTO.getAbout());
        }
        if (userDTO.getAddress() != null && (user.getAddress() == null || !user.getAddress().equals(userDTO.getAddress()))) {
            user.setAddress(userDTO.getAddress());
        }
        if(userDTO.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        }
        return user;
    }

    private User writeAllFields(User user, UserDTO userDTO) {
        user.setLogin(userDTO.getLogin());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRole(userDTO.getRole());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setAbout(userDTO.getAbout());
        user.setAddress(userDTO.getAddress());
        return user;
    }
}
