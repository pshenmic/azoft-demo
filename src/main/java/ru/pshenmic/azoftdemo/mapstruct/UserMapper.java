package ru.pshenmic.azoftdemo.mapstruct;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {


    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "firstName", target = "firstName"),
            @Mapping(source = "lastName", target = "lastName"),
            @Mapping(source = "dateOfBirth", target = "dateOfBirth"),
            @Mapping(source = "login", target = "login"),
            @Mapping(source = "about", target = "about"),
            @Mapping(source = "address", target = "address"),
            @Mapping(source = "role", target = "role"),
            @Mapping(target = "password", ignore = true)
    })
    UserDTO userToUserDTO(User user);
}
