package ru.pshenmic.azoftdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pshenmic.azoftdemo.dto.UserDTO;
import ru.pshenmic.azoftdemo.entity.User;
import ru.pshenmic.azoftdemo.mapstruct.UserListMapper;
import ru.pshenmic.azoftdemo.mapstruct.UserMapper;

import java.util.List;


//One service for different mappers
@Service
public class MappingService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserListMapper userListMapper;

    public UserDTO userToUserDTO(User user) {
        return userMapper.userToUserDTO(user);
    }

    public List<UserDTO> userListToDTOList(List<User> users) {
        return userListMapper.userListToDTOList(users);
    }

}
