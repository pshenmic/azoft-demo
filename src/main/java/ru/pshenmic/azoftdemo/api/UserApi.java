package ru.pshenmic.azoftdemo.api;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pshenmic.azoftdemo.dto.UserDTO;

import java.util.List;

@RequestMapping(value = "/api")
public interface UserApi {

    /**
     * Allowed roles : ROLE_ADMIN
     *
     * Returns {UserDTO} with data about created {User}
     *
     * 200 : OK
     * 400: Bad Request. Fields validation failed
     * 409: Conflict. User with such login is already exists.
     * 401: Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @param userDTO {UserDTO} with data about creating user
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user",
            method = RequestMethod.POST)
    ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO);

    /**
     * Allowed roles : ROLE_ADMIN
     *
     * Returns {List<UserDTO>} with all users from database
     *
     * 200 : {List<UserDTO>} OK
     * 401: Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @return {List<UserDTO>}
     */
    @RequestMapping(value = "/user/getAll",
            method = RequestMethod.GET)
    ResponseEntity<List<UserDTO>> getAllUsers();


    /**
     * Allowed roles : ROLE_ADMIN
     *
     * Returns {UserDTO} of the user by the given id
     *
     * 200 : OK
     * 404 : Not Found. User is not found by the given id
     * 401 : Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @param id {Long}
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.GET)
    ResponseEntity<UserDTO> getUserById(@PathVariable Long id);

    /**
     * Allowed roles : ROLE_ADMIN
     *
     * Modifies user by the given id. Returns {UserDTO} of the modified {User}
     *
     * 200 : OK
     * 404 : Not Found. User is not found by the given id
     * 400 : Bad Request. Fields validation failed
     * 401 : Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @param id      {Long}
     * @param userDTO {UserDTO}
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.PUT)
    ResponseEntity<UserDTO> modifyUserById(@PathVariable Long id, @RequestBody UserDTO userDTO);


    /**
     * Allowed roles : ROLE_ADMIN
     *
     * Deletes user by the given id.
     *
     * 200 : OK
     * 404 : Not Found. User is not found by the given id
     * 401 : Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @param id {Long}
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user/{id}",
            method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteUserById(@PathVariable Long id);


    /**
     * Allowed roles : [ROLE_ADMIN, ROLE_USER]
     *
     * Returns {UserDTO} of the currently authenticated user
     *
     * 200 : OK
     * 401 : Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user",
            method = RequestMethod.GET)
    ResponseEntity<UserDTO> getAuthenticatedUser();

    /**
     * Allowed roles : [ROLE_ADMIN, ROLE_USER]
     *
     * Modifies the currently authenticated user. Returns {UserDTO} of the modified user
     *
     * 200 : OK
     * 400 : Bad Request. Fields validation failed
     * 401 : Unauthorized. Only allowed to ROLE_ADMIN
     *
     * @param userDTO {UserDTO}
     * @return {UserDTO}
     */
    @RequestMapping(value = "/user",
            method = RequestMethod.PUT)
    ResponseEntity<UserDTO> modifyAuthenticatedUser(@RequestBody UserDTO userDTO);

}
