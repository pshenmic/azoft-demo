package ru.pshenmic.azoftdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("ru.pshenmic.azoftdemo")
@EnableAutoConfiguration
public class AzoftDemo {

    public static void main(String[] args) {
        SpringApplication.run(AzoftDemo.class, args);
    }
}
