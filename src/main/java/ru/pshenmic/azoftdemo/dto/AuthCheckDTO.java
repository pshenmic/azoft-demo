package ru.pshenmic.azoftdemo.dto;

public class AuthCheckDTO {

    private boolean authenticated;

    private String role;

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
